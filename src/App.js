import './App.css';
import styled from 'styled-components'
import React, {useRef, useState} from 'react'
import {sendPostRequest, sendGetRequest, sendDeleteRequest, sendUpdateRequest} from './api.to.server'
import List from './List'

const App = ()=> {

  const first_name = useRef(null);
  const last_name = useRef(null);
  const email = useRef(null);
  const phone = useRef(null);
  const deleteRef = useRef(null);
  const updateRef = useRef(null);

  

  const [users_list, set_users_list] = useState([]);
  const [is_display, set_is_display] = useState(true);
  const [delete_toggle, set_delete_toggle] = useState(false);
  const [update_toggle, set_update_toggle] = useState(false);


  // console.log('curr object:', user_form_object);

  const handleSubmit = (e)=> {
    e.preventDefault();
    if (first_name.current.value && 
        last_name.current.value && 
        email.current.value && 
        phone.current.value) {
          const user_object_to_post = {
            first_name: first_name.current.value, 
            last_name: last_name.current.value, 
            email: email.current.value, 
            phone: phone.current.value
          };
        // console.log('need to send:',JSON.stringify(user_object_to_post));
        sendPostRequest(user_object_to_post);
    }
  }

  const handleGetAll = async(e)=> {
    e.preventDefault();
    const newList = await sendGetRequest();
    console.log('newList is:', newList);
    set_users_list(newList);
  }

  const handleDelete = async(e)=> {
    e.preventDefault();
    const resp = await sendDeleteRequest(deleteRef.current.value);
    console.log(resp);
    const newList = await sendGetRequest();
    console.log('newList is:', newList);
    set_users_list(newList);
    
  }


  const handleUpdate = async(e)=> {
    e.preventDefault();
    // if (first_name.current.value && 
    //     last_name.current.value && 
    //     email.current.value && 
    //     phone.current.value) {
      const user_object_to_post = {
        first_name: first_name.current.value, 
        last_name: last_name.current.value, 
        email: email.current.value, 
        phone: phone.current.value
      };
        // console.log('need to send:',JSON.stringify(user_object_to_post));
        const resp = await sendUpdateRequest(user_object_to_post, updateRef.current.value);
        console.log(resp);
        const newList = await sendGetRequest();
        console.log('newList is:', newList);
        set_users_list(newList);
    // }
  }


  return (
    <div className="App">
      {console.log('render jsx -> ', users_list)}

    <MainForm>
      <h2>Interact with Server</h2>
       <label> first name </label>
       <input ref={first_name} onChange={()=> console.log(first_name.current.value)}></input>
       <label> last name </label>
       <input ref={last_name} onChange={()=> console.log(last_name.current.value)}></input>
       <label> email </label>
       <input ref={email} onChange={()=> console.log(email.current.value)}></input>
       <label> phone </label>
       <input ref={phone} onChange={()=> console.log(phone.current.value)}></input>
       <Button onClick={handleSubmit}>Add/Post</Button>
      <Button onClick={handleGetAll}>Get all Users</Button>
      <Button onClick={(e)=> {
        e.preventDefault(0);
        set_delete_toggle(!delete_toggle)
      }}>Delete user</Button>

      {delete_toggle && <>
        <input ref={deleteRef} placeholder='enter user id for delete'></input>
        <button onClick={handleDelete}>Submit Delete</button> </>}

        <Button onClick={(e)=> {
        e.preventDefault(0);
        set_update_toggle(!update_toggle);
      }}>Update user</Button>

      {update_toggle && <>
        <input ref={updateRef} placeholder='enter user id for update'></input>
        <button onClick={handleUpdate}>Submit Update</button> </>}  

    </MainForm>

    {users_list.length > 0 && <Button onClick={()=>set_is_display(!is_display)}>toggle list</Button>}
    {users_list.length > 0 && <List currList={users_list} isDisplay={is_display}/>}

    </div>
  );
}

export default App;

const MainForm = styled.form`
  // background-color: red;
  // margin-top: 18rem;
  position: relative;

  height: 22rem;
  display: flex;
  flex-direction: column;
  width: 25rem;
  padding: 2rem;
  background-color: white;
  box-shadow: 0rem 2rem 4rem rgb(0 0 0 / 30%);
  // align-items: center;
  // text-align: center;

  input {
    // background-color: blue;
  }

`

const Button = styled.button`
 margin: 0.5rem 0;
 width: 50%

`