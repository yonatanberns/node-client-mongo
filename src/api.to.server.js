import axios from 'axios'

export const sendPostRequest = async (data) => {
    console.log('in send post',data);
    try {
        const resp = await axios.post('http://localhost:3030/api/users/', data);
        console.log('recived post-->',resp.data);
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
  };

export const sendGetRequest = async () => {
    try {
        const resp = await axios.get('http://localhost:3030/api/users/');
        console.log('recived get-->',resp.data);
        return resp.data;
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
};

export const sendDeleteRequest = async (id) => {
    try {
        const resp = await axios.delete('http://localhost:3030/api/users/' + id);
        console.log('recived get-->',resp.data);
        return resp.data;
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
};


export const sendUpdateRequest = async (data, id) => {
    console.log('in send put',data);
    try {
        const resp = await axios.put('http://localhost:3030/api/users/' + id, data);
        console.log('recived put-->',resp.data);
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
};