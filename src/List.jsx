import React from "react";
import Card from "./Card";
import styled from "styled-components";


const List = ({currList, isDisplay}) => {

  

  return (
    <Box>

      {isDisplay && <ul>
        {currList.map((itm) => (
          <CardItem key={Math.random(1000)}>
            <Card {...itm} />
            {/* first name: {itm.first_name} */}
          </CardItem>
        ))}
      </ul>}

    </Box>
  );
};
export default List;

const Box = styled.div`
//   background: oldlace;
//   height: 70vh;
//   min-width: 62rem;
  /*border: #0000b9 solid 3px;*/
//   border: 1px solid black;
  position: absolute;
  width: 29rem;
//   overflow-x: hidden;
//   overflow-y: scroll;
//   box-shadow: 0 0.2rem 0.8rem DimGrey;
`;

const CardItem = styled.li`
  &:nth-child(even) {
    background: honeydew;
  }
  &:nth-child(odd) {
    background: white;
  }
`;
