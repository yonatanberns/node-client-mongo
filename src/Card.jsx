import React from "react";
// import Thumb from "./Thumb";
import styled from "styled-components";

const Card = ({ _id,first_name, last_name, email, phone }) => {
  // console.log('first_name:',first_name)
  return (
    <div>
        <h4>id:{_id}</h4>
        <Title>
          First name: {first_name}, Last name: {last_name}
        </Title>
        <Msg>email: {email}, phone: {phone}</Msg>
    
    </div>
  );
};
export default Card;


const Msg = styled.p`
  font-family: "Raleway";
  font-size: 1.1rem;
//   max-width: 7rem;
`;

const Title = styled.h1`
  font-size: 1.1rem;
  color: slategray;
  font-family: "Expletus Sans";

`;

// const TextsBox = styled.div`
// //   padding-left: 1rem;
// `;
